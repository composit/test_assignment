#pragma once

const int win = 5;
const int lose = 1;
const int cooperate = 4;
const int anticooperate = 2;
const int ProbabilityOfErrorProcent = 5;
const int AmountLoosers = 12; //20% from 60
const int MinDeals = 5;
const int MaxDeals = 10;

#define LOG(x) { \
	cout << "LOG: " << #x << " = " << x << endl;	\
	}
