#pragma once

#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <iomanip>
#include "global.h"

using namespace std;

enum class TypeTrader{
	Altruist, Pitcher, Cunning, Unpredictable, Vindictive, Quirky, Winner, Forgiving
};

class AbstractTrader{
public:
	int coins;
	mt19937* gen;
	TypeTrader type_trader;

	void AddCoins(int profit);
	void ResetCoins();
	int GetCoins() const;
	TypeTrader GetType() const;
	string GetTypeStr() const;
	bool Move(const vector<bool>& opponent_moves);
	virtual bool MoveWithoutError(const vector<bool>& opponent_moves) const{
		cout << "AbstractTrader" << endl;
		return true;
	}
	virtual ~AbstractTrader(){}
};

class Trader : public AbstractTrader{
public:
	Trader(mt19937& g, TypeTrader tp);
private:
	//��������
	bool Altruist() const;
	//������
	bool Pitcher() const;
	//������
	bool Cunning(const vector<bool>& opponent_moves) const;
	//���������������
	bool Unpredictable() const;
	//�����������
	bool Vindictive(const vector<bool>& opponent_moves) const;
	//�����
	bool Quirky(const vector<bool>& opponent_moves) const;
	//���������
	bool Forgiving(const vector<bool>& opponent_moves) const;

	bool MoveWithoutError(const vector<bool>& opponent_moves) const;
};

bool operator < (const AbstractTrader& lhs, const AbstractTrader& rhs);


