#include <iostream>
#include <ctime>
#include <tuple>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <iomanip>
#include <memory>
#include <random>

using namespace std;

#include "global.h"
#include "class_trader.h"
#include "class_winner.h"

bool debug = false;

tuple<int, int> CalculateCoins(bool move1, bool move2){
	if (move1 && move2) return make_tuple(cooperate, cooperate);
	else if (move1 && !move2) return make_tuple(lose, win);
	else if (!move1 && move2) return make_tuple(win, lose);
	else return make_tuple(anticooperate, anticooperate);
}

void Deal(shared_ptr<AbstractTrader> tr1, shared_ptr<AbstractTrader> tr2, mt19937& gen){
	if (debug) cout << tr1->GetTypeStr() << " : " << tr2->GetTypeStr() << endl;
	vector<bool> tr1_moves, tr2_moves;
	bool move1, move2;
	int profit1, profit2;
	uniform_int_distribution<int> uid(MinDeals, MaxDeals);
	int amount_deals = uid(gen);
	for (int i = 0; i < amount_deals; i++){
		move1 = tr1->Move(tr2_moves);
		move2 = tr2->Move(tr1_moves);
		tr1_moves.push_back(move1);
		tr2_moves.push_back(move2);
		tie(profit1, profit2) = CalculateCoins(move1, move2);
		tr1->AddCoins(profit1);
		tr2->AddCoins(profit2);
		if (debug) cout << move1 << " : " << move2 <<
				" (" << profit1 << " : " << profit2 << ")" << endl;
	}
	if (debug) cout << tr1->GetCoins() << " - " << tr2->GetCoins() << "\n\n";
}

void Deals(vector<shared_ptr<AbstractTrader>>& trader, mt19937& gen){
	for (size_t i = 0; i < trader.size(); i ++){
		for (size_t j = i + 1; j < trader.size(); j ++){
			Deal(trader[i], trader[j], gen);
		}
	}
}

void Info(int year, vector<shared_ptr<AbstractTrader>> trader){
	map<string, int> amount_kind_of_traders;
	for (const auto& t : trader){
		amount_kind_of_traders[t->GetTypeStr()]++;
	}
	cout << "Year " << (year + 1) << endl;
	for (const auto& i : amount_kind_of_traders){
		cout << setw(15) << i.first << " " << i.second << endl;
	}
	cout << endl;
}

void InfoForMostSuccesfull(vector<shared_ptr<AbstractTrader>> trader){
	map<string, int> amount_kind_of_traders;
	for (auto t : trader){
		amount_kind_of_traders[t->GetTypeStr()]++;
	}
	int max = 0;
	string name;
	for (const auto& i : amount_kind_of_traders){
		if (max < i.second) {
			max = i.second;
			name = i.first;
		}
	}
	cout << "Most successful trader is " << name << endl;
}

void InfoForCoins(vector<shared_ptr<AbstractTrader>> trader){
	for (auto i : trader){
		cout << i->GetTypeStr() << " " << i->GetCoins() << endl;
	}
}

void ReplaceTraders(vector<shared_ptr<AbstractTrader>>& trader, mt19937& gen){
	trader.erase(trader.begin(), trader.begin() + AmountLoosers);
	TypeTrader most_successful = trader.back()->GetType();
	for (int i = 0; i < AmountLoosers; i++){
		shared_ptr<AbstractTrader> t;
		if (most_successful != TypeTrader::Winner){
			t = make_shared<Trader>(gen, most_successful);
		} else {
			t = make_shared<Winner>(gen);
		}
		trader.push_back(t);
	}
}

void SortTraders(vector<shared_ptr<AbstractTrader>>& trader){
	sort(begin(trader),
			end(trader),
			[](shared_ptr<AbstractTrader> l, shared_ptr<AbstractTrader> r){
				return l->GetCoins() < r->GetCoins();
			}
		);
}

void ResetCoinsForEachTrader(vector<shared_ptr<AbstractTrader>>& trader){
	for (auto& t : trader){
		t->ResetCoins();
	}
}

void Tasks_1_2(){
	mt19937 gen(time(0));
	vector<TypeTrader> kind_traders = {TypeTrader::Altruist, TypeTrader::Pitcher,
			TypeTrader::Cunning, TypeTrader::Unpredictable, TypeTrader::Vindictive, TypeTrader::Quirky};
	vector<shared_ptr<AbstractTrader>> trader;

	//__init_vector__
	for (const auto& kind_trader : kind_traders){
		for (int i = 0; i < 10; i++){
			shared_ptr<AbstractTrader> t;
			t = make_shared<Trader>(gen, kind_trader);
			trader.push_back(t);
		}
	}

	//__Deals and replace__
	for (int year = 0; year < 20; year++){
		Deals(trader, gen);
		Info(year, trader);
		SortTraders(trader);
		ReplaceTraders(trader, gen);
		ResetCoinsForEachTrader(trader);
	}

	//__Finish_Info__
	InfoForMostSuccesfull(trader);
}

vector<bool> Training(const vector<shared_ptr<AbstractTrader>>& trader, mt19937& gen){
	vector<shared_ptr<AbstractTrader>> training_vector;
	for (auto i : trader){
		if (i->GetType() != TypeTrader::Winner){
			training_vector.push_back(i);
		}
	}

	auto w_trained = make_shared<Winner>(gen);
	w_trained->CreateMovesVector();

	for (auto tr : training_vector){
		for (int i = 0; i < pow(2, MaxDeals); i++){
			w_trained->ChangeTypeForTrening(i);
			Deal(w_trained, tr, gen);
			w_trained->FillMap();
		}
	}
	w_trained->FindBestMoves();
	return w_trained->GetMoves();
}

vector<shared_ptr<AbstractTrader>> GetVectorWithImprovedWinners(
		const vector<shared_ptr<AbstractTrader>>& trader,
		vector<bool> ImprovedMoves, mt19937& gen){

	vector<shared_ptr<AbstractTrader>> VectorWithImprovedWinners;
	for (auto tr : trader){
		shared_ptr<AbstractTrader> t;
		if (tr->GetType() != TypeTrader::Winner){
			t = tr;
		} else {
			auto w = make_shared<Winner>(gen);
			w->SetMoves(ImprovedMoves);
			t = w;
		}
		VectorWithImprovedWinners.push_back(t);
	}
	return VectorWithImprovedWinners;
}


void Task_3(){
	mt19937 gen(time(0));
	vector<TypeTrader> kind_traders = {TypeTrader::Altruist, TypeTrader::Pitcher,
			TypeTrader::Cunning, TypeTrader::Unpredictable, TypeTrader::Vindictive, TypeTrader::Quirky};

	//__init_vector__
	vector<shared_ptr<AbstractTrader>> trader;
	for (const auto& kind_trader : kind_traders){
		for (int i = 0; i < 10; i++){
			auto t = make_shared<Trader>(gen, kind_trader);
			trader.push_back(t);
		}
	}
	for (int i = 0; i < 10; i++){
		auto w = make_shared<Winner>(gen);
//		w->SetMoves(w_trained->GetMoves());
		trader.push_back(w);
	}

	//__deals, replace and training__
	for (int year = 0; year < 25; year++){
		auto moves = Training(trader, gen);
		ResetCoinsForEachTrader(trader);
		trader = GetVectorWithImprovedWinners(trader, moves, gen);
		Deals(trader, gen);
		Info(year, trader);
		SortTraders(trader);
//		InfoForCoins(trader);
		ReplaceTraders(trader, gen);
		ResetCoinsForEachTrader(trader);
	}

	//__Finish_Info__
	InfoForMostSuccesfull(trader);

}

void Tasks_3_with_forgiving_trader(){
	mt19937 gen(time(0));
	vector<TypeTrader> kind_traders = {TypeTrader::Altruist, TypeTrader::Pitcher,
			TypeTrader::Cunning, TypeTrader::Unpredictable, TypeTrader::Vindictive, TypeTrader::Quirky, TypeTrader::Forgiving};
	vector<shared_ptr<AbstractTrader>> trader;

	//__init_vector__
	for (const auto& kind_trader : kind_traders){
		for (int i = 0; i < 10; i++){
			shared_ptr<AbstractTrader> t;
			t = make_shared<Trader>(gen, kind_trader);
			trader.push_back(t);
		}
	}

	//__Deals and replace__
	for (int year = 0; year < 50; year++){
		Deals(trader, gen);
		Info(year, trader);
		SortTraders(trader);
		ReplaceTraders(trader, gen);
		ResetCoinsForEachTrader(trader);
	}

	//__Finish_Info__
	InfoForMostSuccesfull(trader);
}

int main(){

	//__Solution_of_tasks_1_and_2__
	Tasks_1_2();

	cout << "-------------------------------------------" << endl;

	//__Try_to_solv_the_3d_task_with_class_winner__
	Task_3();

	cout << "-------------------------------------------" << endl;

	//__Solving_the_3d_task_with_strategy_of_forgiving_(Jesus tactic)__
	Tasks_3_with_forgiving_trader();



}
