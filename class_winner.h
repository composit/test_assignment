#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <random>

#include "class_trader.h"

using namespace std;

template<typename T>
ostream& operator << (ostream& os, const vector<T>& v);
template<typename F, typename S>
ostream& operator << (ostream& os, const map<F, S>& m);

class Winner : public AbstractTrader{
public:
	Winner(mt19937& g);
	void CreateMovesVector();
	bool TrainingMove(const vector<bool> opponent_moves)const;
	void ChangeTypeForTrening(int type);
	void FillMap();
	void PrintMap()const;
	void FindBestMoves();
	vector<bool> GetMoves() const;
	void SetMoves(vector<bool> v);
	void Info()const;
	void FillAllVariantsInMap();

private:
	int _type;
	bool is_trained;
	map<vector<bool>, int> moves_coins;
	vector<vector<bool>> vector_for_education;
	vector<bool> moves;

	bool MoveWithoutError(const vector<bool>& opponent_moves)const;
	vector<bool> CreateVectorMoves(int n);
};

template<typename T>
ostream& operator << (ostream& os, const vector<T>& v){
	bool flag = false;
	os << '[';
	for (const auto& i : v){
		if (flag) os << ", ";
		flag = true;
		os << i;
	}
	os << ']';
	return os;
}

template<typename F, typename S>
ostream& operator << (ostream& os, const map<F, S>& m){
	os << '{';
	for (const auto& item : m){
		os << item.first << " " << item.second << endl;;
	}
	os << '}';
	return os;
}
