#include "class_trader.h"

void AbstractTrader::AddCoins(int profit){
	coins += profit;
}
void AbstractTrader::ResetCoins(){
	coins = 0;
}
int AbstractTrader::GetCoins() const{
	return coins;
}
TypeTrader AbstractTrader::GetType() const{
	return type_trader;
}
string AbstractTrader::GetTypeStr() const{
	if (type_trader == TypeTrader::Altruist) return "Altruist";
	else if (type_trader == TypeTrader::Pitcher) return "Pitcher";
	else if (type_trader == TypeTrader::Cunning) return "Cunning";
	else if (type_trader == TypeTrader::Unpredictable) return "Unpredictable";
	else if (type_trader == TypeTrader::Vindictive) return "Vindictive";
	else if (type_trader == TypeTrader::Quirky) return "Quirky";
	else if (type_trader == TypeTrader::Winner) return "Winner";
	else if (type_trader == TypeTrader::Forgiving) return "Forgiving";
	else {
		throw logic_error("don't existed type " + to_string(static_cast<int>(type_trader)));
	}
}
bool AbstractTrader::Move(const vector<bool>& opponent_moves){
	bool move = MoveWithoutError(opponent_moves);
	uniform_int_distribution<int> uid(1, 100);
	return (uid(*gen) > ProbabilityOfErrorProcent) ? move : !move;
}

//----------------------------------------------------------------------------------

Trader::Trader(mt19937& g, TypeTrader tp){
	gen = &g;
	type_trader = tp;
	coins = 0;
}


//��������
bool Trader::Altruist() const{
	return true;
}

//������
bool Trader::Pitcher() const{
	return false;
}

//������
bool Trader::Cunning(const vector<bool>& opponent_moves) const{
	if (opponent_moves.size() == 0){
		return true;
	} else {
		return opponent_moves.back();
	}

}

//���������������
bool Trader::Unpredictable() const{
	uniform_int_distribution<int> uid(0, 1);
	return uid(*gen);
}

//�����������
bool Trader::Vindictive(const vector<bool>& opponent_moves) const{
	for (auto const& move : opponent_moves){
		if (move == false) return false;
	}
	return true;
}

//�����
bool Trader::Quirky(const vector<bool>& opponent_moves) const{
	int count = opponent_moves.size();
	switch (count){
	case 0: return true;
	case 1: return false;
	case 2: return true;
	case 3: return true;
	}
	for (auto const& move : opponent_moves){
		if (move == false) return Pitcher();
	}
	return Cunning(opponent_moves);
}
//���������
bool Trader::Forgiving(const vector<bool>& opponent_moves) const{
	int count = opponent_moves.size();
	switch (count){
	case 0: return true;
	case 1: return true;
	}

	if (opponent_moves.at(count-1) == false && opponent_moves.at(count-2) == false){
		return false;
	} else {
		return true;
	}
}
bool Trader::MoveWithoutError(const vector<bool>& opponent_moves) const{
	if (type_trader == TypeTrader::Altruist) return Altruist();
	else if (type_trader == TypeTrader::Pitcher) return Pitcher();
	else if (type_trader == TypeTrader::Cunning) return Cunning(opponent_moves);
	else if (type_trader == TypeTrader::Unpredictable) return Unpredictable();
	else if (type_trader == TypeTrader::Vindictive) return Vindictive(opponent_moves);
	else if (type_trader == TypeTrader::Quirky) return Quirky(opponent_moves);
	else if (type_trader == TypeTrader::Forgiving) return Forgiving(opponent_moves);
	else {
		throw logic_error("don't existed type " + to_string(static_cast<int>(type_trader)));
	}
}


bool operator < (const AbstractTrader& lhs, const AbstractTrader& rhs){
	return lhs.GetCoins() < rhs.GetCoins();
}





