#include "class_winner.h"

Winner::Winner(mt19937& g){
	gen = &g;
	_type = 0;
	coins = 0;
	is_trained = false;
	type_trader = TypeTrader::Winner;
}
void Winner::CreateMovesVector(){
	for (int i = 0; i < pow(2, MaxDeals); i++){
		vector<bool> v = CreateVectorMoves(i);
		moves_coins[v] = 0;
		vector_for_education.push_back(v);
	}
}
bool Winner::TrainingMove(const vector<bool> opponent_moves)const{
	int num_move = opponent_moves.size();
	return vector_for_education[_type][num_move];
}
void Winner::ChangeTypeForTrening(int type){
	_type = type;
}
void Winner::FillMap(){
	moves_coins[vector_for_education[_type]] += GetCoins();
	ResetCoins();
}
void Winner::PrintMap()const{
	int i = 0;
	vector<bool> i_max;
	int max = 0;
	vector<bool> i_min;
	int min = 1000;
	for (const auto& item : moves_coins){
		cout << i << " ";
		cout << item.first << " " << item.second << endl;
		i++;
		if (max < item.second) {i_max = item.first; max = item.second;}
		if (min > item.second) {i_min = item.first; min = item.second;}
	}
	cout << "MAX: " << "\n" << i_max << " " << moves_coins.at(i_max) << endl;
	cout << "MIN: " << "\n" << i_min << " " << moves_coins.at(i_min) << endl;
}
void Winner::FindBestMoves(){
	vector<bool> best_moves;
	int max = 0;
	for (const auto& item : moves_coins){
		if (max < item.second) {best_moves = item.first; max = item.second;}
	}
	moves = best_moves;
}
vector<bool> Winner::GetMoves() const{
	return moves;
}
void Winner::SetMoves(vector<bool> v){
	moves = v;
	is_trained = true;
}
void Winner::Info()const{
	cout << moves << endl;
	cout << is_trained << endl;
}
void Winner::FillAllVariantsInMap(){
	map<vector<bool>, int> additional;
	for (const auto& item : moves_coins){
		auto begin_it = begin(item.first);
		for (auto end_it = begin(item.first) + 1; end_it != end(item.first); end_it++){
			additional[{begin_it, end_it}] += item.second;
		}
	}
	moves_coins.insert(begin(additional), end(additional));
}

bool Winner::MoveWithoutError(const vector<bool>& opponent_moves)const {
	if (!is_trained){
		int num_move = opponent_moves.size();
		return vector_for_education[_type][num_move];
	} else {
		return moves[opponent_moves.size()];
	}
}

vector<bool> Winner::CreateVectorMoves(int n){
	vector<bool> m(MaxDeals);
	for (int i = 0; i < MaxDeals; i++){
		int mod_2 = n % 2;
		m[i] = (bool)mod_2;
		n /= 2;
	}
	return m;
}


